package org.m2iformation.glpi.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TestAdministration {
	public static ChromeDriver driver;
	
	@Before
	public void setup(){
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://demo.glpi-project.org/index.php");
	}
	
	@Test
	public void ajouterOrdinateur(){

		driver.findElement(By.id("login_name")).sendKeys("admin");
		driver.findElement(By.id("login_password")).sendKeys("admin");
		new Select(driver.findElement(By.name("language"))).selectByVisibleText("Fran�ais");
		driver.findElement(By.className("submit")).click();
		assertTrue(driver.findElement(By.tagName("body")).getText().contains("Accueil"));
		driver.findElement(By.linkText("Parc")).click();
		driver.findElement(By.linkText("Ordinateurs")).click();
		driver.findElement(By.xpath("//a[@title='Ajouter']")).click();
		driver.findElement(By.linkText("Gabarit vide")).click();
		String varOrdinateur=getRandomReference();
		String varSerie=getRandomReference();
		driver.findElement(By.name("name")).sendKeys(varOrdinateur);
		driver.findElement(By.xpath("//select[@name='users_id_tech']/parent::td")).click();
		driver.findElement(By.xpath("//li[contains(.,'Bell Harley')]")).click();
		driver.findElement(By.xpath("//span[contains(@aria-labelledby,'select2-dropdown_domains_id')]")).click();
		driver.findElement(By.xpath("//*[text()='CAMPUS']")).click();
		driver.findElement(By.name("serial")).sendKeys(varSerie);
		driver.findElement(By.name("add")).click();
		driver.findElement(By.name("globalsearch")).sendKeys(varOrdinateur);
		driver.findElement(By.name("globalsearchglass")).click();
		WebElement listeOrdinateurs = driver.findElement(By.xpath("//table[.//th='Nom']"));
		assertEquals(varOrdinateur, listeOrdinateurs.findElements(By.tagName("tr")).get(1).findElements(By.tagName("td")).get(0).getText());
		assertEquals(varSerie, listeOrdinateurs.findElements(By.tagName("tr")).get(1).findElements(By.tagName("td")).get(4).getText());
		assertTrue(driver.findElement(By.tagName("body")).getText().contains(varSerie));
		assertTrue(driver.findElement(By.tagName("body")).getText().contains(varOrdinateur));
		driver.findElement(By.linkText(varOrdinateur)).click();
		driver.findElement(By.cssSelector("a.fa.fa-sign-out")).click();
		System.out.println("test modif");
	}
	@After
	public void tearDown(){
		driver.quit();
		
	}
	
	 public String getRandomReference(){ 
		 String ref=""; 
		 String allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWYZ"; 
		 int stringLength = 4; 
		 for (int i=0; i<stringLength; i++) { 
		        int rnum = (int) Math.floor(Math.random() * allowedChars.length()); 
		        ref += allowedChars.substring(rnum,rnum+1); 
		    } 
		 ref += "-"; 
		for (int i=0; i<stringLength; i++) { 
		        int rnum = (int) Math.floor(Math.random() * 9); 
		        ref += rnum; 
		    } 
		 return ref;
	} 
}
